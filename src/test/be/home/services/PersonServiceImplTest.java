package be.home.services;

import be.home.dao.PersonDao;
import be.home.entities.Person;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceImplTest {
    private PersonService service;
    private List<Person> persons;
    private Person person;

    @Mock
    private PersonDao dao;

    @Before
    public void setUp() {
        person = new Person("user", "test");
        when(dao.findByFirstname("test")).thenReturn(person);

        persons = new ArrayList<Person>();
        persons.add(person);
        when(dao.findAll()).thenReturn(persons);

        service = new PersonServiceImpl(dao);
    }

    @Test
    public void whenFindAll_shouldReturnListOfPersons() {
        assertThat(service.findAll(), is(persons));
        assertThat(service.findAll().size(), is(1));
    }

    @Test
    public void whenFindByFirstname_shouldReturnCorrectPerson() {
        assertThat(service.findByFirstname("test"), is(person));
    }
}
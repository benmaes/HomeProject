package be.home.dao;

import be.home.entities.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/dao.xml")
@Transactional
public class PersonDaoImplTest {
    @Autowired
    private PersonDao dao;

    @Test
    public void whenFindAll_shouldReturnListOfPersons() {
        assertThat(dao.findAll().size(), is(3));
    }

    @Test
    public void whenFindByFirstname_shouldReturnCorrectPerson() {
        Person person = dao.findByFirstname("Ben");
        assertThat(person, is(notNullValue()));
        assertThat(person.getFirstname(), is("Ben"));
        assertThat(person.getId(), is(1l));
        assertThat(person.getName(), is("Maes"));
    }

    @Test
    public void whenFindByFirstnameHasNoResults_shouldReturnNull() {
        Person person = dao.findByFirstname("bla");
        assertThat(person, is(nullValue()));
    }
}
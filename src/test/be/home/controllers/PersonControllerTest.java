package be.home.controllers;


import be.home.entities.Person;
import be.home.services.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonControllerTest {
    private List<Person> persons;
    private Person person;
    private PersonController controller;

    @Mock
    private PersonService personService;

    @Before
    public void setUp() {
        persons = Collections.emptyList();
        when(personService.findAll()).thenReturn(persons);

        person = new Person("user", "test");
        when(personService.findByFirstname(any(String.class))).thenReturn(person);

        controller = new PersonController(personService);
    }

    @Test
    public void whenFindAll_personViewShouldBeRendered() {
        assertThat(controller.findAll().getViewName(), is("person"));
    }

    @Test
    public void whenFindAll_personsRequestAttributeShouldBeGenerated() {
        assertSame(controller.findAll().getModelMap().get("persons"), persons);
    }

    @Test
    public void whenFindByFirstname_personDetailViewShouldBeRendered() {
        assertThat(controller.findByFirstName("testName").getViewName(), is("personDetail"));
    }

    @Test
    public void whenFindByFirstname_personRequestAttributeShouldBeGenerated() {
        assertSame(controller.findByFirstName("testName").getModelMap().get("person"), person);
    }
}

package be.home.controllers;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class IndexControllerTest {
    private IndexController controller;

    @Before
    public void setUp() {
        controller = new IndexController();
    }

    @Test
    public void whenIndex_indexViewShouldBeRendered() {
        assertThat(controller.index(), is("index"));
    }
}
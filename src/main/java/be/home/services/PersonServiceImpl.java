package be.home.services;

import be.home.dao.PersonDao;
import be.home.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    private final PersonDao personDao;

    @Autowired
    public PersonServiceImpl(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Transactional
    public List<Person> findAll() {
        return personDao.findAll();
    }

    public Person findByFirstname(String firstname) {
        return personDao.findByFirstname(firstname);
    }
}

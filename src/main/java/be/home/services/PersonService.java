package be.home.services;

import be.home.entities.Person;

import java.util.List;

public interface PersonService {
    List<Person> findAll();
    Person findByFirstname(String firstname);
}

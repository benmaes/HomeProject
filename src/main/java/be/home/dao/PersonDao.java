package be.home.dao;

import be.home.entities.Person;

import java.util.List;

public interface PersonDao {
    List<Person> findAll();
    Person findByFirstname(String firstname);
}

package be.home.dao;

import be.home.entities.Person;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class PersonDaoImpl implements PersonDao {

    private EntityManager entityManager;

    public List<Person> findAll() {
        TypedQuery<Person> query = entityManager.createNamedQuery("Person.findAll", Person.class);
        return query.getResultList();
    }

    public Person findByFirstname(String firstname) {
        TypedQuery<Person> query = entityManager.createNamedQuery("Person.findByFirstname", Person.class);
        query.setParameter("firstname", firstname);
        try {
            return query.getSingleResult();
        } catch(NoResultException e) {
            return null;
        }
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}

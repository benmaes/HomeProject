package be.home.controllers;

import be.home.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/persons")
public class PersonController {
    private PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView findAll() {
        return new ModelAndView("person", "persons", personService.findAll());
    }

    @RequestMapping(method = RequestMethod.GET, params = "firstname")
    public ModelAndView findByFirstName(@RequestParam String firstname) {
        return new ModelAndView("personDetail", "person", personService.findByFirstname(firstname));
    }
}

<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:template>
    <jsp:attribute name="header">
        <h1>Persons</h1>
    </jsp:attribute>
    <jsp:body>
        <c:forEach items="${persons}" var="person">
            <c:url var="url" value="/persons">
                <c:param name="firstname" value="${person.firstname}"/>

            </c:url>
            <a href="${url}">${person.firstname} ${person.name}</a><br/>
        </c:forEach>
    </jsp:body>
</t:template>

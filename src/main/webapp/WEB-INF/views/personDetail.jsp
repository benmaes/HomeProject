<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:template>
    <jsp:attribute name="header">
        <h1>${person.firstname} ${person.name}</h1>
    </jsp:attribute>
    <jsp:body>
        <p>Id: ${person.id}</p>
        <c:url var="url" value="/persons"/>
        <a href="${url}">back</a>
    </jsp:body>
</t:template>
